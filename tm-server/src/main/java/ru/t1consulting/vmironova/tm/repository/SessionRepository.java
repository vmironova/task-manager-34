package ru.t1consulting.vmironova.tm.repository;

import ru.t1consulting.vmironova.tm.api.repository.ISessionRepository;
import ru.t1consulting.vmironova.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
