package ru.t1consulting.vmironova.tm.api.repository;

import ru.t1consulting.vmironova.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
